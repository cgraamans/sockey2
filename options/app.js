'use strict';
module.exports = {

	// MariaDB settings
	db: {

		// process.env for all secure settings
		connection:{
			user:process.env._DB_USERNAME,
			password:process.env._DB_PASSWD,
			database:process.env._DB,
			host:'localhost',
			charset:'utf8mb4'
		},
		recursive:{
			interval:300, //ms
			limit:100
		}

	},

	// Socket.IO settings
	sockets: {

		port:9001,
		settings:{
			//ex: transports:['websocket'] 
		},

	},

	// Authorization settings
	auth:{

		timeout:1814400000, //ms
		ttl:60000, //ms
		secretTTL:20000 //ms

	},

	// log settings
	logs:{
		processEnvString:process.env._DEBUG,
	},

};