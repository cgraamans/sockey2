# sockey2

## Requirements

  - Redis  
  - NodeJS 8+  
  - MySQL / MariaDB

## Install

  - npm install  
  - create a database, import `/docs/sql/template.sql`  
  - set environmental variables:

Linux: 

    export _DB_USERNAME=""
    export _DB_PASSWD=""
    export _DB=""
    export TEST_PASSWORD="MoreThan1BeerIsGood!"

## Usage

    node init.js

or

    npx nodemon init.js

### Testing

Standard Test Password: `MoreThan1BeerIsGood!`

    npm test