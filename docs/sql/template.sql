-- --------------------------------------------------------
-- Host:                         tethys
-- Server version:               10.1.23-MariaDB-9+deb9u1 - Raspbian 9.0
-- Server OS:                    debian-linux-gnueabihf
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for universe-mk2
CREATE DATABASE IF NOT EXISTS `sockey2` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `sockey2`;

-- Dumping structure for table universe-mk2.user.bans
CREATE TABLE IF NOT EXISTS `user.bans` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `user_id` int(255) DEFAULT NULL,
  `host` text,
  `reason` text,
  `by_admin` int(255) NOT NULL,
  `dt` int(10) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Dumping data for table universe-mk2.user.bans: ~1 rows (approximately)
/*!40000 ALTER TABLE `user.bans` DISABLE KEYS */;
INSERT IGNORE INTO `user.bans` (`id`, `user_id`, `host`, `reason`, `by_admin`, `dt`, `active`) VALUES
  (1, 13, NULL, 'test ban', 1, 1520295648, 1);
/*!40000 ALTER TABLE `user.bans` ENABLE KEYS */;

-- Dumping structure for table universe-mk2.user.tokens
CREATE TABLE IF NOT EXISTS `user.tokens` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `user_id` int(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `persistent` tinyint(1) NOT NULL DEFAULT '1',
  `dt` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Dumping data for table universe-mk2.user.tokens: ~0 rows (approximately)
/*!40000 ALTER TABLE `user.tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `user.tokens` ENABLE KEYS */;

-- Dumping structure for table universe-mk2.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `email` varchar(128) DEFAULT NULL,
  `password` varchar(128) NOT NULL,
  `api_id` varchar(128) NOT NULL,
  `api_token` varchar(128) DEFAULT NULL,
  `dt_register` int(10) NOT NULL,
  `dt_deactivate` int(10) DEFAULT NULL,
  `active` int(1) NOT NULL DEFAULT '1',
  `auth` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Dumping data for table universe-mk2.users: ~3 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT IGNORE INTO `users` (`id`, `name`, `email`, `password`, `api_id`, `api_token`, `dt_register`, `dt_deactivate`, `active`, `auth`) VALUES
  (13, 'testUserBan', NULL, '169457b454e6b3b18141a642c87c23db4947b04d6d4f1f4a87ee7e0e1da1b789', '841a940f3b83f972584c13fc79468c37549819fb58468601dac0561997354558', NULL, 1520294766, NULL, 1, 0),
  (26, 'testUserAdmin', NULL, '169457b454e6b3b18141a642c87c23db4947b04d6d4f1f4a87ee7e0e1da1b789', '841a940f3b83f972584c13fc79468c37549819fb58468601dac0561997354558', NULL, 1520294766, NULL, 1, 2),
  (28, 'testUserRegular', 'test.normalUser@gmail.com', '169457b454e6b3b18141a642c87c23db4947b04d6d4f1f4a87ee7e0e1da1b789', '841a940f3b83f972584c13fc79468c37549819fb58468601dac0561997354558', NULL, 1520294766, NULL, 1, 0);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
