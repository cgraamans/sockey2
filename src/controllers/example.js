module.exports = (app,$state,socketName,data)=>{

	return new Promise((resolve,reject)=>{

		app.LOG.put('D','example triggered by socket '+socketName); // socket name that this event came in under
		app.LOG.put('D','user logged in:'+(!!$state.user.auth)); // false if not logged in

		resolve(data);

	});

};