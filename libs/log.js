'use strict';

class LogObj {

	constructor(processEnvString) {
		
		this.logs = [];
		this.logItem = {
			
			dt:new Date(),
			timestamp:(new Date()).getTime(),
			type:'E',
			message:'',
		
		};
		this.lvl = processEnvString.toUpperCase();

		if(!this.lvl) {

			this.lvl = options.debugLvl;
		
		}

	}

	put(type,message,toConsole) {

		let logLine = Object.assign({},this.logItem);
		let show = false;

		logLine.type = type.toUpperCase();
		logLine.preamble = ' x ';
		logLine.dt = new Date();
		logLine.timestamp = (new Date()).getTime();
		logLine.message = message;

		if(!['E','D','I'].includes(type)) {
			logLine.type = 'E';
		}

		switch(logLine.type){
			case 'D':
				logLine.preamble = ' . ';
				break;
			case 'I':
				logLine.preamble = ' ✔ ';
				break;
		}

		if(this.lvl ===  logLine.type) show = true;
		if(logLine.type === 'E') show = true;
		if(this.lvl ===  'I' && logLine.type ===  'D') show = true;

		this.logs.push(logLine);
		if(show){

			if(!toConsole){
			
				console.log(logLine.dt + logLine.preamble + logLine.message);
			
			} else {
			
				console.log(message);
			
			}

		}

	}

};

module.exports = options=>{ 

	let envStr = 'I';
	if (options.processEnvString){
		envStr = options.processEnvString;	
	}

	return new LogObj(envStr);

};