module.exports = (app,socket)=>{

	return new Promise((resolve,reject)=>{

		let init = $stateObj=>{

				return new Promise((res,rej)=>{

					var _emit = $stateObj.socket.emit,
						_onevent = $stateObj.socket.onevent;

					$stateObj.user.secret = app.AUTH.token();
					$stateObj.socket.emit = (packet,args,noDecode)=>{

						if(!noDecode){
							args = app.AUTH.jwt.sign(args,$stateObj.user.secret);
						}

						_emit.apply($stateObj.socket,[packet,args]);

					};
					$stateObj.socket.emit('init',{ok:true,secret:$stateObj.user.secret},true);

    				$stateObj.socket.onevent = packet=>{

						if(packet.data){

							if(packet.data.length>1) {

								try {

									let decoded = app.AUTH.jwt.verify(packet.data[1],$stateObj.user.secret);
									packet.data[1] = decoded;

									let passedUserData;
									if(packet.data[1].user){ passedUserData = packet.data[1].user }									
									if(packet.data[1]._user){ passedUserData = packet.data[1]._user }
									if(packet.data[1]._u){ passedUserData = packet.data[1]._u }

									if(passedUserData){

										if(!passedUserData.name || !passedUserData.apiId || !passedUserData.token){

											$stateObj.socket.emit('auth',{
												ok:false,
												msg:'malformed user data',
												data:passedUserData
											});

										} else {

											app.AUTH.verifyToken(passedUserData)
												.then(res=>{

													if(res.ban){

														$stateObj.user.auth = false;
														app.LOG('E','banned user requested access: '+passedUserData.apiId);

														$stateObj.socket.emit('auth',{
															ok:false,
															bans:res.ban
														});

														$stateObj.socket.disconnect();

													}

													if(res.error){

														$stateObj.user.auth = false;
														app.LOG.put('D',res.error);

														$stateObj.socket.emit('auth',{
															ok:false,
															msg:res.error
														});

													}

													if(res.user){

														if($stateObj.user.auth) {

															if($stateObj.user.auth.token !== res.user.token){

																$stateObj.socket.emit('auth',{
																	ok:true,
																	user:{
																		apiId:res.user.apiId,
																		token:res.user.token,
																		authLvl:res.user.auth,
																		name:res.user.name,
																	}
																});

															}

														}

														$stateObj.user.auth = res.user;
														$stateObj.toController(packet.data,'default')
															.then(defaultData=>{

																if(defaultData){

																	packet.data = defaultData;
																	$stateObj.toController(packet.data,'local')
																		.then(localData=>{

																			if(localData){

																				packet.data = localData;
																				_onevent.call($stateObj.socket,packet);	

																			}

																		});

																}

															});

													}

												})
												.catch(e=>{

													app.LOG.put('E','verification error');
													app.LOG.put('E',e,true);

												});

										}

									} else {

										$stateObj.toController(packet.data,'default')
											.then(defaultData=>{

												if(defaultData){

													packet.data = defaultData;
													$stateObj.toController(packet.data,'local')
														.then(localData=>{

															if(localData){

																packet.data = localData;
																_onevent.call($stateObj.socket,packet);	

															}

														});

												}

											});

									}

								} catch(err) {

									app.LOG.put('D','decoding error');
									app.LOG.put('E',err,true);

								}

							}

						}

					};

					res();

				});

		};

		let stateObj = class stateObj {
		
			constructor(){

				this.socket = socket;

				this.user = {

					auth:false,
					host:false,
					secret:false,

				};

				this.timers = {

					intervals:[],
					timeouts:[],

				};

				this.socket.on('disconnect',()=>{

					if(this.timers.intervals.length>0){

						this.timers.intervals.forEach(interval=>{

							clearInterval(interval);

						});
					}		

					if(this.timers.timeouts.length>0){

						this.timers.timeouts.forEach(timeout=>{

							clearTimeout(timeout);

						});
					}
					app.LOG.put('D','user disconnected')

				});

			}

			toController(data,routeCategory) {

				let that = this;
				return new Promise((resolve,reject)=>{

					let route = false,
						err = false;

					for(let rC in app.routes[routeCategory]) {

						if(rC === 'administration' && !that.user.auth) err = false;

						if(!err){

							if(app.routes[routeCategory][rC][data[0]]){

								let foundRoute = app.routes[routeCategory][rC][data[0]];
								if(foundRoute.auth && !that.user.auth) err = 'not logged in';
								if(foundRoute.level && !that.user.auth) err = 'not logged in';
								if(foundRoute.level && that.user.auth){

									if(foundRoute.level>that.user.level) err = 'Not authorized.';
									foundRoute.noRoute = true;

								}
								if(!err) {

									route = foundRoute;	

								}

							}

						}
					
					}
					if(err){

						reject({msg:err});

					} else {

						if(route){

							if(app.controllers[routeCategory][route.controller]){

								app.controllers[routeCategory][route.controller](app,that,data[0],data[1])
									.then(routedData=>{

										if(!routedData){
	
											resolve();
	
										} else {
	
											if(!route.noRoute){

												data[1] = routedData;

											}
											resolve(data);

										}

									})
									.catch(e=>{

										reject({e:e,msg:'error routing to controller'});

									});

							} else {

								resolve(data);

							}
					
						} else {

							resolve(data);

						}

					}

				});

			}

		};

		let host = socket.conn.remoteAddress;
		if (socket.handshake.address.length > 6) {

			host = socket.handshake.address;

		}
		if(socket.request.client._peername) {

			if (socket.request.client._peername.address.length > 6) {

				host = socket.request.client._peername.address;

			}

		}
		if(socket.handshake.headers['x-forwarded-for']) {

			if(socket.handshake.headers['x-forwarded-for'].length > 6) {

				host = socket.handshake.headers['x-forwarded-for'];
			
			}

		}

		app.AUTH.bansByHost(host)
			.then(res=>{

				if(res.length>0){

					socket.emit('error',{msg:'you are banned.',data:res});
					socket.disconnect();

					app.LOG('E','banned user requested access: '+host);

				} else {

					let $state = new stateObj();
					$state.host = host;

					init($state).then(()=>{

							let interval = setInterval(function(){

								$state.user.secret = app.AUTH.token(Math.random());
								$state.socket.emit('init',{ok:true,secret:$state.user.secret},true);	

							},app.options.auth.secretTTL);
							$state.timers.intervals.push(interval);

							resolve($state);

						})
						.catch(e=>{

							reject(e);
						
						});

				}

			})
			.catch(e=>{

				reject(e);
			
			});

	});

}