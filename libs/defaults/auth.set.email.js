module.exports = (app,$state,socketName,data)=>{

	return new Promise((resolve,reject)=>{

		let err = false;
		if(!data.email) err = 'no email';
		if(!$state.user.auth) err = 'not logged in';
		if(!app.AUTH.verifyEmail(data.email)) err = 'invalid email address';

		if(!err) {

			app.DB.q('UPDATE users SET email = ? WHERE id = ?',[data.email,$state.user.auth.id])
				.then(()=>{

					$state.socket.emit(socketName,{ok:true});

				})
				.catch(e=>{

					$state.socket.emit(socketName,{ok:false,msg:'email update failed',e:e});

				});

		} else {

			$state.socket.emit(socketName,{ok:false,msg:err});
		
		}
		resolve(data);

	});

};