module.exports = (app,$state,socketName,data)=>{

	return new Promise((resolve,reject)=>{

		if(data.email){

			let pass = app.AUTH.verifyEmail(data.email);
			$state.socket.emit(socketName,{ok:pass});

		}

	});

};