module.exports = (app,$state,socketName,data)=>{

	return new Promise((resolve,reject)=>{

		if(data.name){

			app.AUTH.verifyName(data.name)
				.then(res=>{
					if(res === true){
						$state.socket.emit(socketName,{ok:true})
					} else {
						$state.socket.emit(socketName,{ok:false,msg:res});
					}
					resolve();
				})
				.catch(e=>{
					$state.socket.emit(socketName,{ok:false,e:e});
					reject(e);
				});

		}

	});

};