// Routing settings

// Switches
// 
// auth 		- boolean	- only allow for authenticated users. (default:false)
// noRoute		- boolean	- allow the socket data to pass through to the app's state for subsequent action (as in an 'after' hook). (default:false)
// immutable	- boolean	- set the data to be immutable when passed to the app's state for subsequent action (default:false)
// level 		- int		- admin level required for controller (default 0);
//							  * if level is set, auth to true

'use strict'
module.exports = {

	// Default controllers
	defaults:{

		"auth.login":{
			controller:"libs/defaults/auth.user.login.js",
		},
		"auth.register":{
			controller:"libs/defaults/auth.user.register.js",
		},
		"auth.logout":{
			controller:"libs/defaults/auth.user.logout.js",
		},
		"auth.get.email":{
			controller:"libs/defaults/auth.get.email.js",
			auth:true,
		},
		"auth.set.email":{
			controller:"libs/defaults/auth.set.email.js",
			auth:true,
			noRoute:true,
		},
		"auth.set.password":{
			controller:"libs/defaults/auth.set.password.js",
			noRoute:true,
			auth:true,
		},
		"auth.verify.name":{
			controller:"libs/defaults/auth.verify.name.js",
		},
		"auth.verify.password":{
			controller:"libs/defaults/auth.verify.password.js",
			noRoute:true,
		},
		"auth.verify.email":{
			controller:"libs/defaults/auth.verify.email.js",
		},
		"test.access":{
			controller:"libs/defaults/test.access.js",
		},

	},

	// // admin controllers
	administration: {

		"admin.ban":{
			controller:"libs/defaults/admin.ban.js",
			level:2,	
		},
		"admin.list.users":{
			controller:"libs/defaults/admin.list.users.js",
			level:1,	
		},
		"admin.list.bans":{
			controller:"libs/defaults/admin.list.bans.js",	
			level:1,			
		},
		"admin.list.online":{
			controller:"libs/defaults/admin.list.online.js",
			level:1,
		},

	}

};