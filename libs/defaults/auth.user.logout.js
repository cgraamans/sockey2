module.exports = (app,$state,socketName,data)=>{

	return new Promise((resolve,reject)=>{

		$state.user.auth = false;
		$state.socket.emit(socketName,{ok:true});
		resolve(data);

	});

};