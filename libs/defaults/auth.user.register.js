module.exports = (app,$state,socketName,data)=>{

	return new Promise((resolve,reject)=>{

		let msg = false;
		if(!data.password) msg = 'no password';
		if(!data.name) msg = 'no password';
		let pTest = app.AUTH.verifyPassword(data.password);
		if(pTest.errors.length>0) msg = 'invalid password';

		if(!msg){

			app.AUTH.verifyName(data.name)
				.then(nameValid =>{

					if(nameValid === true) {

						app.AUTH.userRegister(data)
							.then(()=>{
								app.AUTH.userLogin(data)
									.then(login=>{

										let cont = true;
										if(login.ban){

											$state.user.auth = false;
											app.LOG('E','banned user requested access: '+data.name);

											$state.socket.emit('auth',{
												ok:false,
												bans:login.ban
											});
											cont = false;

										}
										if(login.error){

											$state.user.auth = false;
											app.LOG.put('D',login.error);

											$state.socket.emit('auth',{
												ok:false,
												msg:login.error
											});											
											cont = false;
										
										}
										if(cont && login.user){

											$state.user.auth = login.user;
											$state.socket.emit('auth',{
												ok:true,
												user:{
													apiId:login.user.apiId,
													token:login.user.token,
													auth:login.user.auth,
													name:login.user.name,
												}
											});

										}
										resolve(data);

									})
									.catch(e=>{

										app.LOG.put('E','app.AUTH.userLogin');
										app.LOG.put('E',e,true);
										$state.socket.emit(socketName,{ok:false,msg:'error in '+socketName,e:e});
										resolve();

									});
							})
							.catch(e=>{

								app.LOG.put('E','app.AUTH.userRegister');
								app.LOG.put('E',e,true);
								$state.socket.emit(socketName,{ok:false,msg:'error in '+socketName,e:e});
								resolve();

							});

					} else {

						$state.socket.emit(socketName,{ok:false,msg:'name '+nameValid});
						resolve();

					}

				})
				.catch(e=>{

					app.LOG.put('E','app.AUTH.verifyName');
					app.LOG.put('E',e,true);
					$state.socket.emit(socketName,{ok:false,msg:'error in '+socketName,e:e});
					resolve();

				});

		} else {

			$state.socket.emit(socketName,{ok:false,msg:msg});
			resolve();

		}


	});

};