module.exports = (app,$state,socketName,data)=>{

	return new Promise((resolve,reject)=>{

		if(data.password){

			let pass = app.AUTH.verifyPassword(data.password);
			if(pass.errors.length > 0){
				$state.socket.emit(socketName,{ok:false,msg:pass.errors});
			}
			if(pass.errors.length === 0){
				$state.socket.emit(socketName,{ok:true});
			}

		}

	});

};