'use strict';

let appObj = class App {

	constructor(){

		this.users = [];

		this.instance = {
			numCPUs:1,
			redis:false,
		};

		this.options = require('../options/app');
		
		this.LOG = new require('./log')(this.options.logs);
		this.AUTH = new require('./auth')(this);

		// populate controllers & routes
		this.controllers = {
			local:{},
			default:{}
		};
		this.routes = {
			local:require('../options/routes'),
			default:require('./defaults/routes'),
		};

		for(let trC in this.routes) {

			for(let rC in this.routes[trC]) {

				for(let r in this.routes[trC][rC]){
				
					if(!this.controllers[trC][this.routes[trC][rC][r].controller]) {

						this.controllers[trC][this.routes[trC][rC][r].controller] = require('../'+this.routes[trC][rC][r].controller);
						
					}

				}

			}

		}

	}

	init() {

		var that = this;
		return new Promise((resolve,reject)=>{

			let promiseArray = [];
			promiseArray.push(new Promise((res,rej)=>{

				try {
	
					that.DB = require('./db')(this.options.db);
					that.LOG.put('D',`${process.pid} connected to database`);

					res();

				} catch(e) {

					rej(e);

				}

			}));

			promiseArray.push(new Promise((res,rej)=>{

				try {

					that.io = require('socket.io')(that.options.sockets.port,that.options.sockets.settings);
					if(that.instance.redis) {

						let redisAdapter = require('socket.io-redis');
						that.io.adapter(redisAdapter({ host: 'localhost', port: 6379 }));
						that.LOG.put('D','socket redis adapter enabled');

					}
					that.LOG.put('D',`socket port open on ${that.options.sockets.port} for worker ${process.pid}`);

					res();

				} catch(e) {

					rej(e);

				}

			}));

			Promise.all(promiseArray)
				.then(()=>{

					resolve();

				})
				.catch(e=>{

					reject(e);

				});

		});

	}

}
module.exports = new appObj();