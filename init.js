'use strict';

const cluster = require('cluster');
var app = require('./libs/app.js');
const args = require('minimist')(process.argv.slice(2));

app.instance.numCPUs = require('os').cpus().length;
if(args.cpus) {
	if(args.cpus<=app.instance.numCPUs) app.instance.numCPUs = args.cpus;
}
if(args.redis){
	app.instance.redis = true;
}

if(app.instance.numCPUs>1 && !app.instance.redis) app.LOG.put('E','redis required for broadcasting');
if (cluster.isMaster) {

	for (let i = 0; i < app.instance.numCPUs; i++) {
		var worker = cluster.fork();
	}

} else {

	app.init()
		.then(()=>{

			app.io.on('connection',socket=>{

				app.LOG.put('D',`user connected to ${process.pid}`);

				let connection = require('./libs/state.js')(app,socket);
				connection.then($state=>{

					// $state.socket can still be used as regular service
					// see https://socket.io for how.
					
				})
				.catch(e=>{
					
					app.LOG.put('E',e,true);
				
				});
				
			});

	 	})
	 	.catch(e=>{

	 		console.log(e);

	 	});

}

